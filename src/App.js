import { useEffect, useState } from "react";
import "./App.css";

function App({ store }) {
  // const [, forceUpdate] = useState(0);
  // useEffect(() => {
  //   const unSubscribe = store.Subscribe(() => forceUpdate());
  //   return () => unSubscribe();
  // });

  useEffect(() => {
    console.log(store.getState());
  }, [store.getState()]);

  const handleKeyDown = (event) => {
    console.log(event.target.value);
    if ("Enter" === event.code) {
      store.dispatch({
        type: "ADD-TODO",
        id: 1,
        text: event.target.value,
      });
    }
  };

  const Todos = store.getState().map((todo) => (
    <li
      key={todo.id}
      onClick={() =>
        store.dispatch({
          type: "TOGGLE-TODO",
          id: todo.id,
        })
      }
      style={{ textDecoration: todo.completed ? "line-through" : "none" }}
    >
      {todo.text}
    </li>
  ));
  return (
    <div className="app">
      <div>
        <input onKeyUp={(e) => handleKeyDown(e)} type="text" />
        <ul>{Todos}</ul>
      </div>
    </div>
  );
}

export default App;
